const httpStatus = require('http-status');
const passport = require('passport');
const APIError = require('../utils/APIError');
const ADMIN = 'admin';
const SUPER_ADMIN = 'super_admin';
const LOGGED_USER = '_loggedUser';
const ROLE = [SUPER_ADMIN, ADMIN];
const handleJWT = (req, res, next, roles) => async (err, user, info) => {
  const error = err || info;
  const apiError = new APIError({
    message: { th: 'ไม่ได้รับอนุญาต โทเค่นหมดอายุ', en: 'UNAUTHORIZED TOKEN EXPIRED' },
    status: httpStatus.UNAUTHORIZED,
  });
  try {
    if (error || !user) throw error;
  } catch (e) {
    return next(apiError);
  }
  console.log('===============roles=========', user.sub.id);
  const chackState = await Customer.findOne({ _id: user.sub.id });
  console.log('chackState', chackState.loginState);
  if (chackState.loginState === true) {


    if (user.sub.role === SUPER_ADMIN || user.sub.role === ADMIN) {
      if (!ROLE.includes(user.sub.role)) {
        apiError.status = httpStatus.FORBIDDEN;
        apiError.message = { th: 'ไม่ได้รับอนุญาต', en: 'FORBIDDEN' };
        return next(apiError);
      }
      req.user = user.sub;
      return next();
    } else if (!ROLE.includes(user.sub.role)) {
      apiError.status = httpStatus.FORBIDDEN;
      apiError.message = { th: 'ไม่ได้รับอนุญาต', en: 'FORBIDDEN' };
      return next(apiError);
    }
  } else if (chackState.loginState === false) {
    apiError.status = httpStatus.FORBIDDEN;
    apiError.message = { th: 'ไม่ได้รับอนุญาต', en: 'FORBIDDEN' };
    return next(apiError);
  }
};


exports.LOGGED_USER = LOGGED_USER;
exports.ADMIN = ADMIN;
exports.SUPER_ADMIN = SUPER_ADMIN;
exports.authorize = (role = ROLE) => (req, res, next) =>

  passport.authenticate('jwt', { session: false }, handleJWT(req, res, next, role))(req, res, next);

exports.oAuth = service => passport.authenticate(service, { session: false });