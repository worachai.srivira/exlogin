const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const User = require('../models/user.model');
const moment = require('moment-timezone');
const { jwtExpirationInterval } = require('../../config/vars');
const { v4: uuidv4 } = require('uuid');
const redis = require('redis');
const jwt_decode = require('jwt-decode');

const redisPort = process.env.REDIS_PORT;
const redisHost = process.env.REDIS_HOST;
const redisPassword = process.env.REDIS_PASSWORD;

const redisClient = redis.createClient({
  socket: {
    host: redisHost,
    port: redisPort
  },
  password: redisPassword
});

redisClient.on('error', err => {
  console.log('Error ' + err);
});

redisClient.on("connect", function (error) {
  console.log('redis connection');
});

async function generateTokenResponse(user, accessToken, userType) {
  const tokenType = 'Bearer';
  const expiresIn = moment().add(jwtExpirationInterval, 'minutes');
  return {
    tokenType, accessToken, expiresIn,
  };
}

/**
 * Returns jwt token if registration was successful
 * @public
 */
exports.register = async (req, res, next) => {
  try {
    const { body } = req;
    const userChk = await User.findOne({ userName: body.userName });
    if (userChk) {
      res.status(httpStatus.CONFLICT);
      return res.json({ success: false, data: {}, message: { th: 'ผู้ใช้งานซ้ำ', en: 'Username is dupicate' } });
    }
    const user = await (new User(body)).save();
    const userTransformed = user.transform();
    res.status(httpStatus.CREATED);
    return res.json({ user: userTransformed });

  } catch (error) {
    return next(error);
  }
};

exports.userLogin = async (req, res, next) => {
  try {
    await redisClient.connect();
    const user = await User.findOne({ userName: req.body.userName });
    console.log('user=======>', user);
    if (!user) {
      res.json({ success: false, message: { th: 'ชื่อผู้ใช้ไม่ถูกต้อง', en: 'Incorrect userName' } });
      await redisClient.disconnect();
      return null;
    }
    const verifyPass = await User.verifyPassword(req.body.userName, req.body.password);
    if (!verifyPass) {
      res.json({ success: false, message: { th: 'รหัสผ่านไม่ถูกต้อง', en: 'Incorrect password' } });
      await redisClient.disconnect();
      return null;
    }
    const accessToken = await User.getToken(user);
    const token = await generateTokenResponse(user, accessToken, 'USER');
    user.lastLogin = new Date();
    await user.save();
    const id = uuidv4();
    await redisClient.setEx(id, 3600, accessToken);
    await redisClient.disconnect();
    return res.json({ accessToken: id });
  } catch (error) {
    return next(error);
  }
};

exports.validateToken = async (req, res, next) => {
  try {
    console.log('validateToken');
    const id = req.headers.authorization;
    await redisClient.connect();
    const accToken = await redisClient.get(id);
    if (!accToken) {
      res.json({ success: false, accessToken: 'inActive' });
      await redisClient.disconnect();
      return null;
    }
    //decoded jwt token to text
    var decoded = jwt_decode(accToken);
    console.log('decoded', decoded.sub.userName);
    const data = {
      userName: decoded.sub.userName,
      role: decoded.sub.role
    }
    console.log('data', data);
    await redisClient.disconnect();
    res.json({ success: true, accessToken: 'Active', data });
  } catch (error) {
    next(error);
  }
};

exports.userLogout = async (req, res, next) => {
  try {
    console.log('userLogout');
    await redisClient.connect();
    const id = req.headers.authorization;
    const accToken = await redisClient.get(id);
    console.log('accToken', accToken);
    if (!accToken) {
      res.json({
        success: false, message: { th: 'ไม่ได้รับอนุญาต โทเค่นหมดอายุ', en: 'UNAUTHORIZED TOKEN EXPIRED' },
        status: httpStatus.UNAUTHORIZED,
      });
      await redisClient.disconnect();
      return null;
    }
    const clearToken = await redisClient.del(id);
    await redisClient.disconnect();
    res.json({ success: true, logout: 'success' });
  } catch (error) {
    next(error);
  }
};
