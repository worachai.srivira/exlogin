const Joi = require('joi');

module.exports = {
  // POST /v1/auth/register
  register: {
    body: {
      userName: Joi.string().required(),
      password: Joi.string().required().min(6).max(128),
    },
  },

  // POST /v1/auth/login
  login: {
    body: {
      userName: Joi.string().required(),
      password: Joi.string().required().max(128),
    },
  },


  validateToken: {
    Headers: {
      authorization: Joi.string().required()
    },
  },

};
