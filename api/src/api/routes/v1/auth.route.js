const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/auth.controller');

const {
  login,
  register,
  validateToken,
  refresh,
} = require('../../validations/auth.validation');

const router = express.Router();

router.route('/register')
  .post(validate(register), controller.register);

router.route('/users/login')
  .post(validate(login), controller.userLogin);

  router.route('/users/validateToken')
  .post(controller.validateToken);

router.route('/users/logoutUsers')
  .post(controller.userLogout);

module.exports = router;
