const mongoose = require('mongoose');
const httpStatus = require('http-status');
const { omitBy, isNil } = require('lodash');
const bcrypt = require('bcryptjs');
const moment = require('moment-timezone');
const jwt = require('jwt-simple');
const uuidv4 = require('uuid/v4');
const APIError = require('../utils/APIError');
const { env, jwtSecret, jwtExpirationInterval } = require('../../config/vars');

/**
* User Roles
*/
const roles = ['user', 'admin', 'super_admin'];

/**
 * User Schema
 * @private
 */
const userSchema = new mongoose.Schema({
  userName: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    index: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 128,
  },
  role: {
    type: String,
  },
  fName: {
    type: String,
    index: true,
  },
  lName: {
    type: String,
    index: true,
  },
  phone: {
    type: String,
  },
  active: {
    type: Boolean,
    default: true,
    index: true,
  },
  created: {
    type: Date,
    default: new Date(),
  },
  lastLogin: {
    type: Date,
  },
  createdAt: {
    type: String,
  },
  reset_password_token: {
    type: String,
  },
  reset_password_expires: {
    type: String,
  },
});

// use hash in password
userSchema.pre('save', async function save(next) {
  try {
    if (!this.isModified('password')) return next();

    const rounds = env === 'test' ? 1 : 10;

    const hash = await bcrypt.hash(this.password, rounds);
    this.password = hash;

    return next();
  } catch (error) {
    return next(error);
  }
});

/**
 * Methods
 */
userSchema.method({
  transform() {
    const transformed = {};
    const fields = ['_id', 'userName', 'role'];

    fields.forEach((field) => {
      transformed[field] = this[field];
    });

    return transformed;
  },

  token() {
    const playload = {
      exp: moment().add(jwtExpirationInterval, 'minutes').unix(),
      iat: moment().unix(),
      sub: this._id,
    };
    return jwt.encode(playload, jwtSecret);
  },

  async passwordMatches(password) {
    return bcrypt.compare(password, this.password);
  },
});

// create token jwt
userSchema.statics = {

  roles,

  async  getToken(userResponse) {
    const playload = {
      exp: moment().add(jwtExpirationInterval, 'minutes').unix(),
      iat: moment().unix(),
      sub: {
        id: userResponse._id,
        userName: userResponse.userName,
        role: userResponse.role,
      },
    };
    console.log('playload', playload);
    return jwt.encode(playload, jwtSecret);
  },


  checkPasswordIsWrong(error) {
    if (error.name === 'MongoError' && error.code === 11000) {
      return new APIError({
        message: 'Validation Error',
        errors: [{
          location: 'body',
          messages: ['userName or password is wrong'],
        }],
        status: httpStatus.CONFLICT,
        isPublic: true,
        stack: error.stack,
      });
    }
    return error;
  },
  // check duplicate user
  checkDuplicateUserName(error) {
    if (error.name === 'MongoError' && error.code === 11000) {
      return new APIError({
        message: 'Validation Error',
        errors: [{
          field: 'userName',
          location: 'body',
          messages: ['"userName" already exists'],
        }],
        status: httpStatus.CONFLICT,
        isPublic: true,
        stack: error.stack,
      });
    }
    return error;
  },
  async verifyPassword(userName, password) {
    const user = await this.findOne({ userName });
    const resp = await user.passwordMatches(password);
    return resp;
  },
};

/**
 * @typedef User
 */
module.exports = mongoose.model('User', userSchema);
